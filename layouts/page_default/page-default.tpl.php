<?php
/**
 * @file
 * Template for a site panel layout.
 *
 * This template provides a basic site layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['top']: Top content.
 *   - $content['content_top']: Content above the main content.
 *   - $content['content']: The main content.
 *   - $content['content_bottom']: Content below the main content.
 *   - $content['secondary']: Secondary content.
 *   - $content['tertiary']: Tertiary content.
 *   - $content['bottom']: Bottom content.
 */
?>

<?php if ($content['top']): ?>
<div id="top" class="container">
  <div class="container__inner">
    <div class="grid">
      <?php print $content['top']; ?>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if ($main): ?>
<div id="main" class="container">
  <div class="container__inner">
    <div class="grid">
      <?php if ($primary): ?>
        <div id="primary" class="grid__cell<?php print $primary_classes; ?>">
          <div class="inner">
            <?php if ($content['content_header']): ?>
            <header id="content-header">
              <?php print $content['content_header']; ?>
            </header>
            <?php endif; ?>

            <?php if ($content['content_top']): ?>
            <div id="content-top" class="grid">
              <?php print $content['content_top']; ?>
            </div>
            <?php endif; ?>

            <?php if ($content['content']): ?>
            <div id="content" class="grid">
              <?php print $content['content']; ?>
            </div>
            <?php endif; ?>

            <?php if ($content['content_bottom']): ?>
            <div id="content-bottom" class="grid">
              <?php print $content['content_bottom']; ?>
            </div>
            <?php endif; ?>
          </div>
        </div>
      <?php endif; ?>

      <?php if ($content['secondary']): ?>
        <div id="secondary" class="grid__cell<?php print $secondary_classes; ?>">
          <div class="grid">
            <?php print $content['secondary']; ?>
          </div>
        </div>
      <?php endif; ?>

      <?php if ($content['tertiary']): ?>
        <div id="tertiary" class="grid__cell<?php print $tertiary_classes; ?>">
          <div class="grid">
            <?php print $content['tertiary']; ?>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if ($content['bottom']): ?>
<div id="bottom" class="container">
  <div class="container__inner">
    <div class="grid">
      <?php print $content['bottom']; ?>
    </div>
  </div>
</div>
<?php endif; ?>
