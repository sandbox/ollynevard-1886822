<?php

// Plugin definition
$plugin = array(
  'title' => t('Panels'),
  'category' => t('Site'),
  'icon' => 'site_panels.png',
  'theme' => 'site_panels',
  'css' => 'site_panels.css',
  'regions' => array(
    'header_top' => t('Header top'),
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'content' => t('Content'),
    'footer' => t('Footer'),
    'footer_bottom' => t('Footer bottom'),
  ),
);
