<?php

require_once DRUPAL_ROOT . '/' . drupal_get_path('theme', 'minima') . '/includes/minima.utilities.inc.php';

// Plugin definition
$plugin = array(
  'title' => t('Default'),
  'category' => t('Site'),
  'icon' => 'site_default.png',
  'theme' => 'site_default',
  'css' => 'site_default.css',
  'regions' => array(
    'header_top' => t('Header top'),
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'top' => t('Top'),
    'content_header' => t('Content header'),
    'content_top' => t('Content top'),
    'content' => t('Content'),
    'content_bottom' => t('Content bottom'),
    'secondary' => t('Secondary'),
    'tertiary' => t('Tertiary'),
    'bottom' => t('Bottom'),
    'footer' => t('Footer'),
    'footer_bottom' => t('Footer bottom'),
  ),
);

/**
 * Preprocess variables for site-default.tpl.php
 */
function template_preprocess_site_default(&$variables) {
  minima_get_layout_classes($variables);
}

/**
 * Process variables for page-default.tpl.php
 */
function template_process_site_default(&$variables, $hook) {
  // Trim whitespace from content so we can test if it exists from the template.
  foreach ($variables['content'] as $region => $content) {
    $variables['content'][$region] = trim($content);
  }

  // Is there any primary content?
  $variables['primary'] = FALSE;
  foreach(array('content_header', 'content_top', 'content', 'content_bottom') as $region) {
    if ($variables['content'][$region]) {
      $variables['primary'] = TRUE;
      break;
    }
  }

  // Is there any main content?
  $variables['main'] = FALSE;
  foreach(array('content_header', 'content_top', 'content', 'content_bottom', 'secondary', 'tertiary') as $region) {
    if ($variables['content'][$region]) {
      $variables['main'] = TRUE;
      break;
    }
  }
}
