<?php

/**
 * Get layout classes
 */
function minima_get_layout_classes(&$variables, $content_key = 'content') {
  // Layout classes
  $classes['primary_classes'] = '';
  $classes['secondary_classes'] = ' desk--one-quarter lap--one-third';
  $classes['tertiary_classes'] = ' desk--one-quarter desk--pull--three-quarters';

  if ($variables[$content_key]['secondary'] && $variables[$content_key]['tertiary']) {
    $classes['primary_classes'] = ' desk--one-half';
  }
  elseif ($variables[$content_key]['secondary'] || $variables[$content_key]['tertiary']) {
    $classes['primary_classes'] = ' desk--three-quarters';
  }

  if ($variables[$content_key]['secondary']) {
    $classes['primary_classes'] .= ' lap--two-thirds';
  }
  if ($variables[$content_key]['tertiary']) {
    $classes['primary_classes'] .= ' desk--push--one-quarter';
    $classes['secondary_classes'] .= ' desk--push--one-quarter';
  }

  drupal_alter('minima_layout_classes', $classes, $variables[$content_key]);

  $variables += $classes;
}
