<div class="branding grid__cell">
  <?php print $branding_logo; ?>
  <?php print $branding_name; ?>
  <?php if ($branding_slogan): ?>
    <div class="branding__slogan"><?php print $branding_slogan; ?></div>
  <?php endif; ?>
</div>
